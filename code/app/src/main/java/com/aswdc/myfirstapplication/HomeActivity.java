package com.aswdc.myfirstapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class HomeActivity extends AppCompatActivity {

    Button btnForm;
    Button btnCalculator;
    private long backPressedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        initViewReference();
        initViewEvent();
    }

    @Override
    public void onBackPressed() {
        if (backPressedTime + 3000 > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(getBaseContext(), "Press Back Again To Exit", Toast.LENGTH_SHORT).show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    void initViewReference() {
        btnForm = findViewById(R.id.btnActForm);
        btnCalculator = findViewById(R.id.btnActCalculator);
    }

    void initViewEvent() {
        btnForm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent formIntent = new Intent(HomeActivity.this, FormActivity.class);
                startActivity(formIntent);
            }
        });

        btnCalculator.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent calculatorIntent = new Intent(HomeActivity.this, CalculatorActivity.class);
                startActivity(calculatorIntent);
            }
        });
    }
}
