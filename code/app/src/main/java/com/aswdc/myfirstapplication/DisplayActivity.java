package com.aswdc.myfirstapplication;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.myfirstapplication.adapter.UserListAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class DisplayActivity extends AppCompatActivity {

    ListView lvUsers;
    Spinner spUsers;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;

//    LinearLayout llMain;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_activity);
        initViewReference();
        bindViewValues();
    }

    void bindViewValues() {
        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        userListAdapter = new UserListAdapter(this, userList);
        lvUsers.setAdapter(userListAdapter);


    }

    void initViewReference() {
        lvUsers = findViewById(R.id.lvActUserList);
        getSupportActionBar().setTitle(R.string.lbl_display_activity);
    }
}
