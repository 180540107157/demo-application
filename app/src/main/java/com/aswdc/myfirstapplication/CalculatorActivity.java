package com.aswdc.myfirstapplication;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class CalculatorActivity extends AppCompatActivity {

    TextView tvRow1Col1, tvRow1Col2, tvRow1Col3, tvRow1Col4, tvRow2Col1, tvRow2Col2, tvRow2Col3, tvRow2Col4;
    TextView tvRow3Col1, tvRow3Col2, tvRow3Col3, tvRow3Col4, tvRow4Col1, tvRow4Col2, tvRow4Col3, tvRow4Col4;
    TextView tvRow5Col1, tvRow5Col2, tvRow5Col3, tvRow5Col4, tvRow6Col1, tvRow6Col2, tvRow6Col3, tvRow6Col4;
    TextView tvDisplay1, tvDisplay2;
    Boolean flag = true;
    Boolean flag2 = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator_activity);
        initViewRefrence();
        initViewEvent();
    }

    void initViewRefrence() {
        tvRow1Col1 = findViewById(R.id.tvCalcActRow1Col1);
        tvRow1Col2 = findViewById(R.id.tvCalcActRow1Col2);
        tvRow1Col3 = findViewById(R.id.tvCalcActRow1Col3);
        tvRow1Col4 = findViewById(R.id.tvCalcActRow1Col4);

        tvRow2Col1 = findViewById(R.id.tvCalcActRow2Col1);
        tvRow2Col2 = findViewById(R.id.tvCalcActRow2Col2);
        tvRow2Col3 = findViewById(R.id.tvCalcActRow2Col3);
        tvRow2Col4 = findViewById(R.id.tvCalcActRow2Col4);

        tvRow3Col1 = findViewById(R.id.tvCalcActRow3Col1);
        tvRow3Col2 = findViewById(R.id.tvCalcActRow3Col2);
        tvRow3Col3 = findViewById(R.id.tvCalcActRow3Col3);
        tvRow3Col4 = findViewById(R.id.tvCalcActRow3Col4);

        tvRow4Col1 = findViewById(R.id.tvCalcActRow4Col1);
        tvRow4Col2 = findViewById(R.id.tvCalcActRow4Col2);
        tvRow4Col3 = findViewById(R.id.tvCalcActRow4Col3);
        tvRow4Col4 = findViewById(R.id.tvCalcActRow4Col4);

        tvRow5Col1 = findViewById(R.id.tvCalcActRow5Col1);
        tvRow5Col2 = findViewById(R.id.tvCalcActRow5Col2);
        tvRow5Col3 = findViewById(R.id.tvCalcActRow5Col3);
        tvRow5Col4 = findViewById(R.id.tvCalcActRow5Col4);

        tvRow6Col1 = findViewById(R.id.tvCalcActRow6Col1);
        tvRow6Col2 = findViewById(R.id.tvCalcActRow6Col2);
        tvRow6Col3 = findViewById(R.id.tvCalcActRow6Col3);
        tvRow6Col4 = findViewById(R.id.tvCalcActRow6Col4);

        tvDisplay1 = findViewById(R.id.tvCalcActDisplay1);
        tvDisplay2 = findViewById(R.id.tvCalcActDisplay2);

    }


    void initViewEvent() {/*
        tvRow1Col1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag=true;
            }
        });*/

        tvRow1Col2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        tvRow1Col3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        tvRow1Col4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });


        tvRow2Col1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        tvRow2Col2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        tvRow2Col3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        tvRow2Col4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        tvRow3Col1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

        tvRow3Col2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        tvRow3Col4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        tvRow4Col1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        tvRow4Col2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        tvRow4Col3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        tvRow4Col4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });


        tvRow5Col1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        tvRow5Col2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }

        });

        tvRow5Col3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvDisplay1.length() == 0) {
                    if (flag2) {
                        tvDisplay2.setText("");
                        flag2 = false;
                    }
                    tvDisplay2.setText(tvDisplay2.getText().toString() + "3");
                } else {
                    if (flag) {
                        tvDisplay2.setText("");
                        flag = false;
                    }
                    tvDisplay2.setText("");
                    tvDisplay2.setText(tvDisplay2.getText().toString() + "3");
                }
            }
        });

        tvRow5Col4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });


        tvRow6Col1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        tvRow6Col2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        tvRow6Col3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }

        });

        tvRow6Col4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    void performOperations(String operand, String operator) {
        Double answer = null;
        Double temp1 = Double.parseDouble(operand);
        Integer temp2 = null;

        if (operator.equals("1/x")) {
            tvDisplay1.setText("1/( " + operand + " )");
            answer = 1 / temp1;
        } else if (operator.equals("x^2")) {
            tvDisplay1.setText(operand);
            answer = temp1 * temp1;
        } else if (operator.equals("x^1/2")) {
            tvDisplay1.setText(operand);
            answer = Math.sqrt(temp1);
        } else if (operator.equals("+/-")) {
            if (!tvDisplay2.getText().toString().equals("0")) {
                tvDisplay2.setText("-" + tvDisplay2.getText().toString());
            }
        }

        temp2 = (int) Math.round(answer);

        if (answer - temp2 == 0) {
            tvDisplay2.setText(temp2.toString());
        } else {
            tvDisplay2.setText(answer.toString());
        }

        Log.d("Log:::", "" + temp1);
        Log.d("Log:::", "" + temp2);
        Log.d("Log:::", "" + operator);
        Log.d("Log:::", "" + answer);

    }

    void performOperations(String operand1, String operand2, char operator) {
        Double answer = 0.0;
        Double temp1 = Double.parseDouble(operand1);
        Double temp2 = Double.parseDouble(operand2);
        Integer temp3 = 0;

        if (operator == '+') {
            answer = temp1 + temp2;
        } else if (operator == '-') {
            answer = temp1 - temp2;
        } else if (operator == 'X') {
            answer = temp1 * temp2;
        } else if (operator == '/') {
            answer = temp1 / temp2;
        }

        temp3 = (int) Math.round(answer);

        if (answer - temp3 == 0) {
            tvDisplay1.setText(operand1 + " " + operator + " " + operand2 + " =");
            tvDisplay2.setText(temp3.toString());
        } else {
            tvDisplay1.setText(operand1 + " " + operator + " " + operand2 + " =");
            tvDisplay2.setText(answer.toString());
        }

        Log.d("Log:::", "" + temp1);
        Log.d("Log:::", "" + temp2);
        Log.d("Log:::", "" + temp3);
        Log.d("Log:::", "" + operator);
        Log.d("Log:::", "" + answer);
    }
}



















